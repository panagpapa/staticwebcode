package com.amdocs.web.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.amdocs.Calculator;
import com.amdocs.Increment;
import com.amdocs.config.SpringWebConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringWebConfig.class})
@WebAppConfiguration
public class IncrementTest {

    @Test
    public void decreaseero() throws Exception {

        int k= new Increment().decreasecounter(0);
        assertEquals("Add", 1, k);
    }

@Test
    public void decreaseone() throws Exception {

        int k= new Increment().decreasecounter(1);
        assertEquals("Add", 1, k);
    }

@Test
    public void decreaseenone() throws Exception {

        int k= new Increment().decreasecounter(10);
        assertEquals("Add", 1, k);
    }


}
